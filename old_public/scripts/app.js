
/*----------  LOAD FILE  ----------*/

var context = new AudioContext();
var tender;
var CANVASBG = 'rgb(200, 200, 200)';


/**
 * Appends two ArrayBuffers into a new one.
 * 
 * @param {ArrayBuffer} buffer1 The first buffer.
 * @param {ArrayBuffer} buffer2 The second buffer.
 */
function appendBuffer(buffer1, buffer2) {
  var numberOfChannels = Math.min( buffer1.numberOfChannels, buffer2.numberOfChannels );
  var tmp = context.createBuffer( numberOfChannels, (buffer1.length + buffer2.length), buffer1.sampleRate );
  for (var i=0; i<numberOfChannels; i++) {
    var channel = tmp.getChannelData(i);
    channel.set( buffer1.getChannelData(i), 0);
    channel.set( buffer2.getChannelData(i), buffer1.length);
  }
  return tmp;
}

function init() {


  /**
   * Loads a song
   * 
   * @param {String} url The url of the song.
   */
  function loadSongWebAudioAPI(url) {
    var request = new XMLHttpRequest();

    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    /**
     * Appends two ArrayBuffers into a new one.
     * 
     * @param {ArrayBuffer} data The ArrayBuffer that was loaded.
     */
    function storeAudioBuffer(data) {
      //decode the loaded data
      context.decodeAudioData(data, function(buf) {
        //buf is an AudioBuffer
        tender = buf;

        // var audioSource = context.createBufferSource();
        // audioSource.connect(context.destination);

        // // Concatenate the two buffers into one.
        // audioSource.buffer = appendBuffer(buf, buf);
        // audioSource.start();
        // // audioSource.playbackRate.value = 1;
        // console.log(audioSource);
      });

    };

    // When the song is loaded asynchronously try to play it.
    request.onload = function() {
      console.log("XHR respose");
      console.log(request.response);

      storeAudioBuffer(request.response);
    }

    request.send();
  }


  loadSongWebAudioAPI('conte_2020-04-06_21-05-48.ogg');
}
init();

/*----------  END CUSTOM  ----------*/

// set up basic variables for app

const buttons = document.querySelector('#buttons');
const record = document.querySelector('.record');
const stop = document.querySelector('.stop');
const soundClips = document.querySelector('.sound-clips');
const canvas = document.querySelector('.visualizer');
const mainSection = document.querySelector('.main-controls');

// disable stop button while not recording

stop.disabled = true;

// visualiser setup - create web audio api context and canvas

let audioCtx = context;
const canvasCtx = canvas.getContext("2d");
let mediaRecorderStream;

//main block for doing the audio recording

if (navigator.mediaDevices.getUserMedia) {
  console.log('getUserMedia supported.');

  const constraints = { audio: true };
  let chunks = [];

  let onSuccess = function(stream) {
    const mediaRecorder = new MediaRecorder(stream);

    visualize(stream);
    mediaRecorderStream = stream;

    record.onclick = function() {
      CANVASBG = 'rgb(200, 45, 67)';
      mediaRecorder.start();
      console.log(mediaRecorder.state);
      console.log("recorder started");
      record.style.background = "red";

      stop.disabled = false;
      record.disabled = true;
    }

    stop.onclick = function() {
      mediaRecorder.stop();
      console.log(mediaRecorder.state);
      console.log("recorder stopped");
      record.style.background = "";
      record.style.color = "";
      // mediaRecorder.requestData();

      stop.disabled = true;
      record.disabled = false;
    }

    mediaRecorder.onstop = function(e) {
      console.log("data available after MediaRecorder.stop() called.");

      // const clipName = prompt('Enter a name for your sound clip?','My unnamed clip');

      // const clipContainer = document.createElement('article');
      // const clipLabel = document.createElement('p');
      // const audio = document.createElement('audio');
      // const deleteButton = document.createElement('button');

      // clipContainer.classList.add('clip');
      // audio.setAttribute('controls', '');
      // deleteButton.textContent = 'Delete';
      // deleteButton.className = 'delete';

      // if(clipName === null) {
      //   clipLabel.textContent = 'My unnamed clip';
      // } else {
      //   clipLabel.textContent = clipName;
      // }

      // clipContainer.appendChild(audio);
      // clipContainer.appendChild(clipLabel);
      // clipContainer.appendChild(deleteButton);
      // soundClips.appendChild(clipContainer);

      // audio.controls = true;
      console.log("attempt to create AudioBufferbuffer from record:");

      _fileReader = new FileReader();
      const blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
      chunks = [];

      let concatenatedBuffer;
      _fileReader.onload = () => {
        context.decodeAudioData(_fileReader.result)
            .then((decodedData) => {
              // console.log(decodedData);
              // console.log(tender);

              concatenatedBuffer = appendBuffer(decodedData, tender);

              var audioSource = context.createBufferSource();
              
              audioSource.connect(context.destination);
              audioSource.buffer = concatenatedBuffer;

              
              var streamDestination = context.createMediaStreamDestination();
              audioSource.connect(streamDestination);
              audioSource.start();

              var stream = streamDestination.stream;
              CANVASBG = 'rgb(45, 200, 67)';
              buttons.style.opacity = 0.2;
              visualize(stream);

              console.log("Created concatenated audiosource");
              console.log(audioSource);

              audioSource.onended = () => {
                CANVASBG = 'rgb(200, 200, 200)';
                if (mediaRecorderStream){
                  visualize(mediaRecorderStream);
                }

                buttons.style.opacity = 1;
              }

            }).catch((e) => {
              throw "Could not decode audio data: " + e.name + ". " + e.message;
            });

      }

      _fileReader.readAsArrayBuffer(blob);

      // const audioURL = window.URL.createObjectURL(blob);
      // audio.src = audioURL;
      // console.log("recorder stopped");

      // deleteButton.onclick = function(e) {
      //   let evtTgt = e.target;
      //   evtTgt.parentNode.parentNode.removeChild(evtTgt.parentNode);
      // }

      // clipLabel.onclick = function() {
      //   const existingName = clipLabel.textContent;
      //   const newClipName = prompt('Enter a new name for your sound clip?');
      //   if(newClipName === null) {
      //     clipLabel.textContent = existingName;
      //   } else {
      //     clipLabel.textContent = newClipName;
      //   }
      // }
    }

    mediaRecorder.ondataavailable = function(e) {
      chunks.push(e.data);
    }
  }

  let onError = function(err) {
    console.log('The following error occured: ' + err);
  }

  navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

  console.log("READY FOR MORE")

} else {
   console.log('getUserMedia not supported on your browser!');
}

function visualize(stream) {
  console.log(stream);
  if(!audioCtx) {
    audioCtx = new AudioContext();
  }

  const source = audioCtx.createMediaStreamSource(stream);

  const analyser = audioCtx.createAnalyser();
  analyser.fftSize = 2048;
  const bufferLength = analyser.frequencyBinCount;
  const dataArray = new Uint8Array(bufferLength);

  source.connect(analyser);
  //analyser.connect(audioCtx.destination);

  draw()

  function draw() {
    WIDTH = canvas.width
    HEIGHT = canvas.height;

    requestAnimationFrame(draw);

    analyser.getByteTimeDomainData(dataArray);

    canvasCtx.fillStyle = CANVASBG;
    canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

    canvasCtx.lineWidth = 2;
    canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

    canvasCtx.beginPath();

    let sliceWidth = WIDTH * 1.0 / bufferLength;
    let x = 0;


    for(let i = 0; i < bufferLength; i++) {

      let v = dataArray[i] / 128.0;
      let y = v * HEIGHT/2;

      if(i === 0) {
        canvasCtx.moveTo(x, y);
      } else {
        canvasCtx.lineTo(x, y);
      }

      x += sliceWidth;
    }

    canvasCtx.lineTo(canvas.width, canvas.height/2);
    canvasCtx.stroke();

  }
}

window.onresize = function() {
  canvas.width = mainSection.offsetWidth;
}

window.onresize();


// window.addEventListener('load',init,false);