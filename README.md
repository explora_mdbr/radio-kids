![Build Status](https://gitlab.com/explora_mdbr/radio-kids/badges/master/pipeline.svg)

---
## USAGE:
Record an introduction to a song. It will be played back before starting with the song.

This test is based on [P5.js SoundRecorder](https://p5js.org/reference/#/p5.SoundRecorder) 